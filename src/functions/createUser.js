'use strict';


const AWS = require('aws-sdk');
const { v4: uuidv4 } = require('uuid');
const bcrypt = require('bcryptjs');
const dynamodb = new AWS.DynamoDB.DocumentClient();

module.exports.createUser = async (event, context, callback) => {
  console.log(event)
  const { email, password } = event;


  const queryUserParams = {
    TableName: process.env.USER_TABLE,
    FilterExpression: '#email = :email',
    ExpressionAttributeNames: {
      '#email': 'email'
    },
    ExpressionAttributeValues: {
      ':email': email
    }
  }
  let userResult = {}
  try {
    userResult = await dynamodb.scan(queryUserParams).promise();
    console.log(userResult.Count);
    console.log(userResult);
    if (userResult.Count > 0) {
      callback(new Error('Email is already using'))
    }
  } catch (queryError) {
    console.log('queryUserParams', queryUserParams)
    callback(queryError)
  }



  const hashedPassword = await bcrypt.hash(password, 10)
  const params = {
    TableName: process.env.USER_TABLE,
    Item: {
      id: uuidv4(),
      email,
      password: hashedPassword
    }
  }
  const response = {
    statusCode: 201,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
      'Access-Control-Allow-Headers': 'Authorization'
    },
    body: params.Item
  }

  try {
    await dynamodb.put(params).promise()
    callback(null, response)
  } catch (e) {
    callback(new Error(e))
  }
}